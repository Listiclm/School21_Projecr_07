### 1. GET/api/v1/Activities

URL https://fakerestapi.azurewebsites.net/api/v1/Activities

Ожидаемый результат: HTTP статус: код ответа 200. Запрос выполнен успешно. 
Тело ответа в формате JSON возвращается от сервера. 
Схема JSON отображена корректно, имена и типы полей соответствуют ожидаемым. Каждое поле JSON объекта соответствует аргументу метода сервиса.

Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 2ec4dac2-67ca-4020-9211-e45448af0a69
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса: отсутствует

Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Tue, 20 Jun 2023 11:18:06 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

Тело ответа: 
```
[
{
  "id": 1,
  "title": "Activity 1",
  "dueDate": "2023-06-10T12:55:12.550936+00:00",
  "completed": false
},
...
{
   "id": 30,
   "title": "Activity 30",
   "dueDate": "2023-06-10T12:55:12.5509481+00:00",
   "completed": true
 }
]
```

### 2. GET/api/v1/Activities/{id}

https://fakerestapi.azurewebsites.net/api/v1/Activities/{{LocalActiveID_GET}}

Ожидаемый результат: HTTP статус: код ответа 200. Запрос выполнен успешно.
Тело ответа в формате JSON возвращается от сервера.
Схема JSON отображена корректно, имена и типы полей соответствуют ожидаемым. Каждое поле JSON объекта соответствует аргументу метода сервиса.

Заголовки запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: fa5ef621-141f-41d4-8f61-b1a2dc9b3ebf
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса: отсутствует

Заголовки ответа:

Content-Type: application/json; charset=utf-8; v=1.0
Date: Tue, 20 Jun 2023 12:39:03 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

Тело ответа:
```
{
    "id": 23,
    "title": "Activity 23",
    "dueDate": "2023-06-21T11:39:04.5352438+00:00",
    "completed": false
}
```
### 3. Post/api/v1/Activities

https://fakerestapi.azurewebsites.net/api/v1/Activities

Ожидаемый результат: HTTP статус: код ответа 200. Запрос выполнен успешно.
Тело ответа в формате JSON возвращается от сервера.
Схема JSON отображена корректно, имена и типы полей соответствуют ожидаемым. Каждое поле JSON объекта соответствует аргументу метода сервиса.

Заголовок запроса:

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 2ec4dac2-67ca-4020-9211-e45448af0a69
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-20T14:02:40.642Z",
  "completed": true
}
```
Заголовок ответа:
Content-Type: application/json; charset=utf-8; v=1.0
Date: Tue, 20 Jun 2023 14:18:58 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

Тело ответа:
```
{
    "id": 23,
    "title": "Activity 23",
    "dueDate": "2023-06-21T11:39:04.5352438+00:00",
    "completed": false
}
```

### 4. POST​/api​/v1​/Activities ​ошибочный (null)

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: 404 Not Found

Заголовок запроса:
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 709688c6-57c1-4bc8-b846-e89f29e79641
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса
```
{
  "id": null,
  "title": "string",
  "dueDate": "2023-06-20T14:02:40.642Z",
  "completed": true
}
```
Заголовок ответа
Content-Length: 0
Date: Tue, 20 Jun 2023 21:27:38 GMT
Server: Kestrel

Тело ответа
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-61b1c216c702fc449f8e6f2ea96f9a32-52b82a28100f9f40-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."]}}
```  

### 5. GET ​/api​/v1​/Activities​/{id} в id=0

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

Ожидаемый результат:  Запрос отправлен на сервер, статус-код получен: 404 - Error: Not Found

Заголовок запроса:
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 5028ce6d-bb23-4374-949d-baeca5078364
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive 

Тело запроса отсутствует

Заголовок ответа
Content-Type: application/problem+json; charset=utf-8; v=1.0
Date: Tue, 20 Jun 2023 21:51:43 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

Тело ответа
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Not Found",
    "status": 404,
    "traceId": "00-c4ebb3cdfa5e1a4eb62dc174189e36db-08d6e53bf348a947-00"
}
```

### 6. GET​/api/v1/Activities/{пустое поле}

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}    

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "200 - Получен объект, соответствующий запрошенному ресурсу"    

Request Headers
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 3e7b0834-a753-44ac-a8a3-b3bb3d496707
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive      

Request Body - 

Response Headers
Content-Type: application/json; charset=utf-8; v=1.0
Date: Thu, 15 Jun 2023 10:00:17 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0  

Response Body  
```
[
    {
        "id": 1,
        "title": "Activity 1",
        "dueDate": "2023-06-21T13:38:24.202241+00:00",
        "completed": false
    },
    {
        "id": 30,
        "title": "Activity 30",
        "dueDate": "2023-06-22T18:38:24.2022528+00:00",
        "completed": true
    }
]
```
### 7. Put/api/v1/Activities/{id} на отправление ресурсов активностей с определенным идентификатором

 https://fakerestapi.azurewebsites.net/api/v1/Activities/23 

Ожидаемый результат:  Запрос отправлен на сервер, статус-код получен:   200 - Запрошенное действие выполнено      

Загловок запроса
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 6087a6a2-4a41-4fa9-815e-a8f674819713
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive 

Тело запроса
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-21T11:24:34.112Z",
  "completed": true
}
```
Заголовок ответа
Content-Type: application/json; charset=utf-8; v=1.0
Date: Wed, 21 Jun 2023 11:24:51 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1 

Тело ответа
```
{
    "id": 0,
    "title": "string",
    "dueDate": "2023-06-21T11:24:34.112Z",
    "completed": true
} 
```
### 8. PUT ​/api​/v1​/Activities​/{id} ошибочный (пусто)

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}  

Ожидаемый результат:  Запрос отправлен на сервер, статус-код получен: 405 - Method Not Allowed

Заголовок запроса
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: abb29d34-7ae2-4124-8091-186d3d8076ff
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive  

Тело запроса
```
{
  "id": ,
  "title": "string",
  "dueDate": "2023-06-21T11:24:34.112Z",
  "completed": true
}  
```
Заголовок ответа
Content-Type: application/json; charset=utf-8; v=1.0
Date: Wed, 21 Jun 2023 11:24:51 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0 

Тело ответа
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-5d837f693a9dde419ec7995ac7750993-41d09878b3f0ef40-00","errors":{"id":["The value '2202195600\n' is not valid."]}}
```    

### 9. DELETE ​/api​/v1​/Activities​/{id}, id =10

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}  

Ожидаемый результат:  Запрос отправлен на сервер, статус-код получен: 200 - Запрошенное действие выполнено   

Заголовок запроса
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: b874a3ff-9be6-41ce-9d21-2c08f7f8faa7
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса -

Заголовок ответа
Content-Length: 0
Date: Wed, 21 Jun 2023 12:43:43 GMT
Server: Kestrel
api-supported-versions: 1.0

Тело ответа -

### 10. DELETE ​/api​/v1​/Activities​/{id} ошибочный { id}<=0(отрицательное число)

URL: https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}  

Фактический результат:  Запрос отправлен на сервер, статус-код получен: 200 - Запрошенное действие выполнено

Ожидаемый результат:   Запрос не выполнен,  статус-код получен: 405 (Method Not Allowed - Метод не разрешен)  

Заголовок запроса:
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: fea5cac4-8e58-4b2d-82cf-5685ca129fce
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса -

Заголовок ответа:
Content-Length: 0
Date: Wed, 21 Jun 2023 13:35:44 GMT
Server: Kestrel
api-supported-versions: 1.0

Тело ответа -

### 11. POST​/api/v1/Activities/ null в id в теле запроса

https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activitybad_id_null}}

Фактический результат:  Запрос отправлен на сервер, статус-код получен: 400 -Bad Request

Ожидаемый результат:   Запрос не выполнен,  статус-код получен: 400-Bad Request

Заголовок запроса:
User-Agent: PostmanRuntime/7.32.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 790f6905-9800-438c-874d-9d9d196d98d3
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

Тело запроса -

Заголовок ответа:
Content-Type: application/problem+json; charset=utf-8
Date: Wed, 21 Jun 2023 13:43:04 GMT
Server: Kestrel
Transfer-Encoding: chunked

Тело ответа 
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-cc26b808f79c5a4eb82348917dc696df-a13f858bc1487147-00",
    "errors": {
        "id": [
            "The value 'null' is not valid."
        ]
    }
}
```

### 12. Authors:  POST ​/api​/v1​/Authors

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors     

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "200 - Запрошенное действие выполнено"    

Request Headers
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: cfcf833b-a466-4049-b789-680d41908eb1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive  

Request Body
```
{
  "id": 0,

  "idBook": 0,

  "firstName": "string",

  "lastName": "string"
}  
```
Response Headers
Content-Type: application/json; charset=utf-8; v=1.0
Date: Thu, 15 Jun 2023 10:27:00 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0  

Response Body
```
{"id":0,"idBook":0,"firstName":"string","lastName":"string"}
```

### 13. POST​/api/v1/Authors/{10000000000 или 10 биллионов}

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/{10000000000 или 10 биллионов}

Заголовки запроса: -H  "accept: /"
Статус-код ответа: 400
Тело запроса:
```
{
  "id": 1000000000000000000000,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

Заголовки ответа: access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-type: application/json; charset=utf-8; v=1.0 
 date: Tue20 Jun 2023 09:33:29 GMT 
 server: Kestrel 
 transfer-encoding: chunked 

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Bad Request"  
Тело ответа:
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-2ae35e0e39bbfb49a529e1af98ad36b4-b53f1ba13d19654e-00",
    "errors": {
        "$.id": [
            "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 30."
        ]
    }
}
```
### 14. POST​/api/v1/Authors/ отрицательное число в id в теле запроса

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/
Ожидаемый результат: HTTP статус: 400. Запрос не выполнен. 
Заголовки запроса: -H  "accept: /"

Тело запроса:
```
{
  "id": -100,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Заголовки ответа: access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-type: application/json; charset=utf-8; v=1.0 
 date: Tue20 Jun 2023 09:33:29 GMT 
 server: Kestrel 
 transfer-encoding: chunked 
 Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Bad Request"  
Статус-код ответа: 200
Тело ответа:
```
{
    "id": -100,
    "idBook": 0,
    "firstName": "string",
    "lastName": "string"
}
```

### 15. POST​/api/v1/Authors/ null в id в теле запроса

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/
Ожидаемый результат: HTTP статус: 400. Запрос не выполнен. 
Заголовки запроса: -H  "accept: /"

Тело запроса:
```
{
  "id": null,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Заголовки ответа: access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-type: application/json; charset=utf-8; v=1.0 
 date: Tue20 Jun 2023 09:33:29 GMT 
 server: Kestrel 
 transfer-encoding: chunked 
 Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Bad Request"  
Статус-код ответа: 400
Тело ответа:
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-3e513ea76695794daa230f4d473fa021-a97acd0f3041c441-00",
    "errors": {
        "$.id": [
            "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."
        ]
    }
}
```

### 16. POST​/api/v1/Authors/ *** в id в теле запроса

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/
Ожидаемый результат: HTTP статус: 400. Запрос не выполнен. 
Заголовки запроса: -H  "accept: /"

Тело запроса:
```
{
  "id": ***,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Заголовки ответа: access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-type: application/json; charset=utf-8; v=1.0 
 date: Tue20 Jun 2023 09:33:29 GMT 
 server: Kestrel 
 transfer-encoding: chunked 
 Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Bad Request"  
Статус-код ответа: 400
Тело ответа:
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-3e513ea76695794daa230f4d473fa021-a97acd0f3041c441-00",
    "errors": {
        "$.id": [
            "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."
        ]
    }
}
```

### 17. POST​/api/v1/Authors  в id в теле запроса пусто

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/
Ожидаемый результат: HTTP статус: 400. Запрос не выполнен. 
Заголовки запроса: -H  "accept: /"

Тело запроса:
```
{
  "id": ,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Заголовки ответа: access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-type: application/json; charset=utf-8; v=1.0 
 date: Tue20 Jun 2023 09:33:29 GMT 
 server: Kestrel 
 transfer-encoding: chunked 
 Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Bad Request"  
Статус-код ответа: 400
Тело ответа:
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-3e513ea76695794daa230f4d473fa021-a97acd0f3041c441-00",
    "errors": {
        "$.id": [
            "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 12."
        ]
    }
}
```

 ### 18. GET ​/api​/v1​/Authors​/authors​/books​/{idBook} ошибочный { id}<=0(отрицательное число)

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{activity_id}}  

Фактический результат:  Запрос отправлен на сервер, статус-код получен: 200 - Запрошенное действие выполнено     

Ожидаемый результат:   Запрос не выполнен,  статус-код получен: 405 (Method Not Allowed - Метод не разрешен)   

Request Headers
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: a3b651a2-fc1d-43ad-a8db-18f421bbf0fb
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive  

Request Body -  

Response Headers
Content-Type: application/json; charset=utf-8; v=1.0
Date: Thu, 15 Jun 2023 10:43:39 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0  

Response Body  
```
[]   
```

### 19. Authors: GET ​/api​/v1​/Authors​/authors​/books​/{idBook} ошибочный

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{activity_id}}  

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Error: Bad Request"      

Request Headers
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: b203f2a8-4c22-4c07-8045-9362446963ed
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive  

Request Body -  

Response Headers
Content-Type: application/problem+json; charset=utf-8
Date: Thu, 15 Jun 2023 10:40:00 GMT
Server: Kestrel
Transfer-Encoding: chunked  
Статус-код ответа: 400

Response Body
```
{"type":
"https://tools.ietf.org/html/rfc7231#section-6.5.1",
"title":"One or more validation errors occurred.",
"status":400,"traceId":"00-58686fc65ec83a49b7a5b5bc92769d5c-34926f471009154c-00",
"errors":{
"idBook":["The value '8008008008' is not valid."]}}  
```

 ### 20. Authors: GET ​/api​/v1​/Authors​/authors​/books​/{idBook}

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{activity_id}}  

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "200 - Получен объект, соответствующий запрошенному ресурсу"    

Request Headers
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: c2f95193-40e4-4fa3-97d9-e5b0b35087f5
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive  

Request Body -  

Response Headers
Content-Type: application/json; charset=utf-8; v=1.0
Date: Thu, 15 Jun 2023 10:33:00 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0  

Response Body
```
[{"id":242,"idBook":80,"firstName":"First Name 242","lastName":"Last Name 242"},{"id":243,"idBook":80,"firstName":"First Name 243","lastName":"Last Name 243"},{"id":244,"idBook":80,"firstName":"First Name 244","lastName":"Last Name 244"}]  
```

### 21. GET/api/v1/Authors/authors/books/{***}

URL: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{***}  

Ожидаемый результат: Запрос отправлен на сервер, статус-код получен: "400 - Error: Bad Request"      

Request Headers
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: b203f2a8-4c22-4c07-8045-9362446963ed
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive  

Request Body -  

Response Headers
Content-Type: application/problem+json; charset=utf-8
Date: Thu, 15 Jun 2023 10:40:00 GMT
Server: Kestrel
Transfer-Encoding: chunked  
Статус-код ответа: 400

Response Body:
```
{"type":
"https://tools.ietf.org/html/rfc7231#section-6.5.1",
"title":"One or more validation errors occurred.",
"status":400,"traceId":"00-58686fc65ec83a49b7a5b5bc92769d5c-34926f471009154c-00",
"errors":{
"idBook":
["The value '8008008008' is not valid."]
}}  
```


--
Отправлено из мобильной Яндекс Почты
